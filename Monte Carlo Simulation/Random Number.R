seed = 2018
n_sample <- 10000

par(mfrow=c(2,2))
# normal distribution #
set.seed(seed)
output_AllAtOnce <- rnorm(n_sample, mean = 0, sd =1)
hist(output_AllAtOnce, freq = TRUE, main = 'Normal Distribution \n (sampling mutiple)')

set.seed(seed)
output_OneByOne <- vector()
for (i in 1:n_sample){
        output_OneByOne <- c(output_OneByOne,
                             rnorm(1, mean = 0, sd = 1))
}
hist(output_OneByOne, freq = TRUE, main = 'Normal Distribution \n (multiple sampling)')

# lognorm distribution #
set.seed(seed)
output_AllAtOnce <- rlnorm(n_sample, meanlog = 0, sdlog = 1)
hist(output_AllAtOnce, freq = TRUE, main = 'Log Norm \n (sampling once)')

set.seed(seed)
output_OneByOne <- vector()
for (i in 1:n_sample){
        output_OneByOne <- c(output_OneByOne,
                             rlnorm(1, meanlog = 0, sdlog = 1))
}
hist(output_OneByOne, freq = TRUE, main = 'Log Norm \n (mutiple sampling)')
